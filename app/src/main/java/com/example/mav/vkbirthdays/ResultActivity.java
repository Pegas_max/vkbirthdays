package com.example.mav.vkbirthdays;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ArrowKeyMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by mav on 27.02.2015.
 */
public class ResultActivity extends Activity {
    private static final int PICK_CONTACT_REQUEST = 1;

    TextView resultText;
    Button exitButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result);
        resultText = (TextView) findViewById(R.id.splash_text);
        resultText.setMovementMethod(ArrowKeyMovementMethod.getInstance());
        exitButton = (Button) findViewById(R.id.to_list_btn);

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), BirthdayListActivity.class);
                startActivityForResult(intent, PICK_CONTACT_REQUEST);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_CONTACT_REQUEST) {
            exitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            if (resultCode == RESULT_OK) {
                switch (data.getIntExtra("Success code", 42)){
                    case 0:{
                        resultText.setText(getString(R.string.splash_added_text) + data.getStringExtra("result"));
                        exitButton.setText(R.string.splash_exit);
                        break;
                    }
                    case 1:{
                        resultText.setText(getString(R.string.splash_no_friends_text));
                        exitButton.setText(R.string.splash_exit);
                        break;
                    }
                    case 2:{
                        resultText.setText(R.string.splash_on_access_denied_text);
                        exitButton.setText(R.string.splash_exit);
                        break;
                    }
                    case 3:{
                        resultText.setText("Please, press button below");
                        exitButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(getApplication(), BirthdayListActivity.class);
                                startActivityForResult(intent, PICK_CONTACT_REQUEST);
                            }
                        });
                    }
                }

            } else {
                resultText.setText(getString(R.string.splash_something_gone_wrong_text));
                exitButton.setText(R.string.splash_exit);
            }
        } else {
            resultText.setText(getString(R.string.splash_something_gone_wrong_text));
            exitButton.setText(R.string.splash_exit);
        }
    }
}
