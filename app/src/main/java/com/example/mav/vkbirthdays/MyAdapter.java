package com.example.mav.vkbirthdays;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mav on 03.03.2015.
 */
public class MyAdapter extends BaseAdapter {

    private Context context;
    private List<Friend> friends = new ArrayList<>();;
    private int addCounter = 0;
    private OnCheckedFriendsListener listener;

    public MyAdapter(Context context) {
        this.context = context;
    }

    public void setData (List<Friend> friendList){
        this.friends.clear();
        this.friends = friendList;
        this.notifyDataSetChanged();
    }

    public void setListener(OnCheckedFriendsListener listener) {
        this.listener = listener;
    }

    public int getCount() {
        return friends.size();
    }

    public Object getItem(int id) {
        return friends.get(id);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolderItem viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.friends_list_layout, parent, false);

            viewHolder = new ViewHolderItem();
            viewHolder.nameViewItem = (TextView) convertView.findViewById(R.id.name_text);
            viewHolder.dateViewItem = (TextView) convertView.findViewById(R.id.date_text);
            viewHolder.checkBoxItem = (CheckBox) convertView.findViewById(R.id.check);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        String name = friends.get(position).getFriendName();
        Date date = friends.get(position).getBirthDate();
        SimpleDateFormat dateFormat = friends.get(position).getDateFormat();
        String birthDate = dateFormat.format(date) + "";
        boolean checked = friends.get(position).isChecked();
        viewHolder.nameViewItem.setText(name);
        viewHolder.dateViewItem.setText(birthDate);
        viewHolder.checkBoxItem.setChecked(checked);
        viewHolder.checkBoxItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("VK", friends.get(position).getFriendName() + " Checked is " + viewHolder.checkBoxItem.isChecked());
                friends.get(position).setChecked(viewHolder.checkBoxItem.isChecked());
                incAddCounter(viewHolder.checkBoxItem.isChecked());
            }
        });
        return convertView;
    }

    public void incAddCounter(boolean add) {
        addCounter += add ? 1 : -1;

        switch (addCounter) {
            case 0: {
                listener.btnVisible(false);
                break;
            }
            case 1:
                listener.btnVisible(true);
                break;
            default:
                break;
        }
    }

    static class ViewHolderItem {
        TextView nameViewItem;
        TextView dateViewItem;
        CheckBox checkBoxItem;
    }
}