package com.example.mav.vkbirthdays;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;
import com.vk.sdk.api.model.VKUsersArray;
import com.vk.sdk.dialogs.VKCaptchaDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class BirthdayListActivity extends Activity implements OnCheckedFriendsListener {

    private String sTokenKey = "VK_ACCESS_TOKEN";
    private String TAG_APP = "VK_APP";
    private String TAG_SDK = "VKSdk";
    private String usernam = "";
    private String SUCCESS_CODE_KEY = "Success code";
    private String RESULT_KEY = "result";

    // fixme в качестве ссылоной переменной лучше испльзовать интерфейсы
    private final ArrayList<Friend> friends = new ArrayList<>();
    com.vk.sdk.VKSdkListener listener = new com.vk.sdk.VKSdkListener() {

        @Override
        public void onAcceptUserToken(VKAccessToken token) {
            Log.d(TAG_SDK, "onAcceptUserToken " + token);
            loadFriends();
        }

        @Override
        public void onReceiveNewToken(VKAccessToken newToken) {
            Log.d(TAG_SDK, "onRecieveNewToken " + newToken);
            newToken.saveTokenToSharedPreferences(getApplicationContext(), sTokenKey);
            loadFriends();
        }

        @Override
        public void onCaptchaError(VKError captchaError) {
            new VKCaptchaDialog(captchaError).show();
            Log.d(TAG_SDK, "onCaptchaError");
        }

        public void onTokenExpired(VKAccessToken expiredToken) {
            VKSdk.authorize();
            Log.d(TAG_SDK, "onTokenExpired");
        }

        @Override
        public void onAccessDenied(VKError authorizationError) {
            Log.d(TAG_SDK, "onAccessDenied");
            sendResultBack(2, null);
        }

        public void onRenewAccessToken(VKAccessToken token) {
            Log.d(TAG_SDK, "onRenewAccessToken");
            sendResultBack(3, null);
            loadFriends();
        }
    };

    private VKRequest currentRequest;
    private ListView listView;
    private Button downloadBtn;
    private ProgressBar progressBar;
    private MyAdapter listAdapter;
    private AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.ac_birthday_list);

        downloadBtn = (Button) findViewById(R.id.download_btn);
        downloadBtn.setVisibility(View.GONE);

        progressBar = (ProgressBar) findViewById(R.id.birthday_list_progress);
        progressBar.setVisibility(View.GONE);

        listView = (ListView) findViewById(R.id.friend_list);
        listView.setVisibility(View.GONE);
        listAdapter = new MyAdapter(this);
        listView.setAdapter(listAdapter);
        listAdapter.setListener(this);

        VKSdk.initialize(listener, getString(R.string.VK_APP_ID));
        VKUIHelper.onCreate(this);

        downloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeListOfChosen();
            }
        });

//        if (!VKSdk.wakeUpSession()){
//            VKSdk.authorize(VKScope.FRIENDS);
//        }
//        loadFriends();
        helloDialog();
    }

    private void helloDialog(){
        builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_old_guy_title);
        builder.setPositiveButton(getString(R.string.dialog_old_guy_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                VKSdk.wakeUpSession();
                loadFriends();
            }
        });

        if (VKSdk.isLoggedIn()){
            builder.setNegativeButton(getString(R.string.dialog_old_guy_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    removeToken();
                    VKSdk.logout();
//                    helloDialog();
                    VKSdk.authorize(VKScope.FRIENDS);
                    loadFriends();
//                    sendResultBack(3, null);
                }
            });
            getUserName();
        }else {
            builder.setMessage(getString(R.string.dialog_first_time_message));
            builder.setPositiveButton(getString(R.string.dialog_first_time_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    VKSdk.authorize(VKScope.FRIENDS);
                    loadFriends();
                }
            });
            builder.show();
        }
    }

    private void removeToken(){
        VKAccessToken.removeTokenAtKey(this, sTokenKey);
    }

    private void getUserName(){
        VKApi.users().get().executeWithListener(new VKRequest.VKRequestListener() {

            @Override
            public void onComplete(VKResponse response) {
                VKApiUser user = ((VKList<VKApiUser>)response.parsedModel).get(0);
                Log.d(TAG_APP,user.first_name + " " + user.last_name);
                usernam = user.first_name + " " + user.last_name;
                builder.setMessage(getString(R.string.dialog_old_guy_message, usernam));
                builder.show();
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                super.attemptFailed(request, attemptNumber, totalAttempts);
                usernam = "неизвестный пользователь";
                builder.setMessage(getString(R.string.dialog_old_guy_message, usernam));
                builder.show();
                Log.d(TAG_APP, "attemptFailed" + request + " " + attemptNumber + " " + totalAttempts);
            }
        });
    }

    private void loadFriends() {

        final String FRIENDS_REQUEST = "id,first_name,last_name,bdate";
        progressBar.setVisibility(View.VISIBLE);

        if (currentRequest != null) {
            currentRequest.cancel();
        }
        currentRequest = VKApi.friends().get(VKParameters.from(VKApiConst.FIELDS, FRIENDS_REQUEST));
        currentRequest.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                Log.d(TAG_APP, "onComplete " + response);
                VKUsersArray friendsArray = (VKUsersArray) response.parsedModel;
                friends.clear();
                SimpleDateFormat formatDMY = new SimpleDateFormat("dd.MM.yyyy");
                SimpleDateFormat formatDM = new SimpleDateFormat("dd.MM");
                for (VKApiUserFull userFull : friendsArray) {
                    Date birthDate = null;
                    SimpleDateFormat format = null;
                    if (!TextUtils.isEmpty(userFull.bdate)) {

                        try {
                            format = formatDMY;
                            birthDate = format.parse(userFull.bdate);
                        } catch (Exception ignored) {
                        }

                        if (birthDate == null) {
                            try {
                                format = formatDM;
                                birthDate = format.parse(userFull.bdate);
                            } catch (Exception e) {
                                Log.e(TAG_APP, "There is no suitable format");
                                break;
                            }
                        }

                        friends.add(new Friend(userFull.first_name + " " + userFull.last_name, birthDate, format));
                    }
                }

            listAdapter.setData(friends);
            progressBar.setVisibility(View.GONE);
            if (friends.size()>0){
                listView.setVisibility(View.VISIBLE);
            } else {
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendResultBack(1, null);
                    }
                });

                builder.setMessage(getString(R.string.dialog_no_friends_message));
                builder.show();
            }
        }

            @Override
            public void onError(VKError error) {
                super.onError(error);
                Log.d(TAG_APP, "onError " + error);
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                super.attemptFailed(request, attemptNumber, totalAttempts);
                Log.d(TAG_APP, "attemptFailed" + request + " " + attemptNumber + " " + totalAttempts);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
    }

    private void makeListOfChosen() {
        List<Friend> addedFriends = new ArrayList<>();
        int size = friends.size();
        for (int i = 0; i < size; i++) {      //is any picked friends?
            if (friends.get(i).isChecked()) {
                addedFriends.add(friends.get(i));
            }
        }
            makeEvents(addedFriends);                   //start to interact with GCalendar
    }

    private void makeEvents(List<Friend> list) {

        java.util.Calendar calendar = java.util.Calendar.getInstance(java.util.TimeZone.getDefault(), java.util.Locale.getDefault());
        calendar.setTime(new java.util.Date());
        final int currentYear = calendar.get(java.util.Calendar.YEAR);
        final SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyy");
        final String FREQ = "FREQ=YEARLY";
        final String at = " at ";

        String result = "";

        int i;
        int size = list.size();
        for (i = 0; i < size; i++) {
            long calID = 1;

            Friend friend = list.get(i);
            String name = friend.getFriendName();
            Calendar calendarBirthDate = Calendar.getInstance();
            calendarBirthDate.setTime(friend.getBirthDate());
            String descrEnding = SDF.equals(friend.getDateFormat()) ? at + calendarBirthDate.get(Calendar.YEAR) : "" ;

            Calendar beginTime = Calendar.getInstance();
            beginTime.set(currentYear, calendarBirthDate.get(Calendar.MONTH), calendarBirthDate.get(Calendar.DAY_OF_MONTH), 8, 30);
            Calendar endTime = Calendar.getInstance();
            endTime.set(currentYear, calendarBirthDate.get(Calendar.MONTH), calendarBirthDate.get(Calendar.DAY_OF_MONTH), 14, 30);
            TimeZone tz = TimeZone.getDefault();
            Calendar lastDate = Calendar.getInstance();
            lastDate.set(2045, calendarBirthDate.get(Calendar.MONTH), calendarBirthDate.get(Calendar.DAY_OF_MONTH), 8, 30);

            ContentResolver cr = getContentResolver();
            ContentValues values = new ContentValues();
            values.put(CalendarContract.Events.RRULE, FREQ);
            values.put(CalendarContract.Events.DTSTART, beginTime.getTimeInMillis());
            values.put(CalendarContract.Events.DTEND, endTime.getTimeInMillis());
            values.put(CalendarContract.Events.LAST_DATE, lastDate.getTimeInMillis());
            values.put(CalendarContract.Events.TITLE, getString(R.string.login_birthday, name));
            values.put(CalendarContract.Events.DESCRIPTION, getString(R.string.login_was_borned,name,descrEnding));
            values.put(CalendarContract.Events.CALENDAR_ID, calID);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, tz.toString());
            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

            result += name + "\n";
        }
        result += getString(R.string.login_to_splash_added, i );
        sendResultBack(0, result);
    }

    @Override
    public void btnVisible(boolean isVisible) {
        downloadBtn.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    private void sendResultBack(int successCode, String result){
        Intent returnIntent = new Intent();
        returnIntent.putExtra(SUCCESS_CODE_KEY, successCode);
        returnIntent.putExtra(RESULT_KEY, result);
        setResult(RESULT_OK, returnIntent);
        finish();
    }
}
