package com.example.mav.vkbirthdays;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mav on 26.02.2015.
 */
public class Friend {
    private String friendName;
    private Date birthDate;
    private final SimpleDateFormat dateFormat;
    private boolean checked;

    public Friend(String friendName, Date birthDate, SimpleDateFormat dateFormat) {
        this.friendName = friendName;
        this.birthDate = birthDate;
        this.dateFormat = dateFormat;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(Boolean newChecked) {
        checked = newChecked;
    }

    public String getFriendName() {
        return friendName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public SimpleDateFormat getDateFormat() {
        return dateFormat;
    }
}
